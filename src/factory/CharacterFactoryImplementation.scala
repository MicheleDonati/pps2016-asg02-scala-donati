package factory

import characters.BasicGameCharacter
import characters.Mario
import characters.Mushroom
import characters.Turtle
import strategies.ContactCharacterStrategy

class CharacterFactoryImplementation() extends CharacterFactory {

  def createMario(x: Int, y: Int): BasicGameCharacter = {
    new Mario(x, y)
  }

  def createMushroom(x: Int, y: Int): BasicGameCharacter = {
    new Mushroom(x, y)
  }

  def createTurtle(x: Int, y: Int): BasicGameCharacter = {
    new Turtle(x, y)
  }
}
