package factory

import characters.{BasicGameCharacter, Mario, Mushroom, Turtle}
import strategies.ContactCharacterStrategy

trait CharacterFactory {
  def createMario(x: Int, y: Int): BasicGameCharacter

  def createMushroom(x: Int, y: Int): BasicGameCharacter

  def createTurtle(x: Int, y: Int): BasicGameCharacter
  
}
