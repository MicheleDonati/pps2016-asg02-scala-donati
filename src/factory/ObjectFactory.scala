package factory

import objects.GameObject

trait ObjectFactory {
  def createBlock(x: Int, y: Int): GameObject

  def createPiece(x: Int, y: Int): GameObject

  def createTunnel(x: Int, y: Int): GameObject

  def createGrowUpMushroom(x: Int, y: Int): GameObject
}

