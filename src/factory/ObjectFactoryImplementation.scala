package factory

import objects._

class ObjectFactoryImplementation extends ObjectFactory {
  def createBlock(x: Int, y: Int): GameObject = {
    new Block(x, y)
  }

  def createPiece(x: Int, y: Int): GameObject = {
    new Piece(x, y)
  }

  def createTunnel(x: Int, y: Int): GameObject = {
    new Tunnel(x, y)
  }

  override def createGrowUpMushroom(x: Int, y: Int) : GameObject = {
    new GrowUpMushroom(x,y)
  }
}