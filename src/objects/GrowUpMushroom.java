package objects;

import utils.Resources;
import utils.Utils;

import java.awt.*;

/**
 * Created by Michele on 11/04/2017.
 */
public class GrowUpMushroom extends GameObject implements Runnable {

    public GrowUpMushroom(int x, int y) {
        super(x, y, Resources.GROW_UP_WIDTH, Resources.GROW_UP_HEIGHT);
        super.imageObject = Utils.getImage(Resources.IMG_GROW_UP);
    }

    @Override
    public Image imageOnMovement() {
        return null;
    }

    @Override
    public void run() {
        while (true) {
            this.imageOnMovement();
            try {
                Thread.sleep(Resources.PAUSE);
            } catch (InterruptedException e) {
            }
        }
    }


}
