package game

import characters._
import factory._
import java.awt.Graphics
import java.awt.Graphics2D
import java.awt.Image
import java.util
import javax.swing.JPanel
import objects.GameObject
import strategies._
import utils.Resources
import utils.Utils
import scala.collection.JavaConversions._
import scala.collection.mutable


@SuppressWarnings(Array("serial")) class Platform private[game]() extends JPanel {
  private var background1PositionX: Int = Resources.POSITION_X_BACKGROUND1
  private var background2PositionX: Int = Resources.POSITION_X_BACKGROUND2
  private var movement: Int = Resources.MOVEMENT
  private var xPosition: Int = Resources.X_POSITION
  private var floorOffsetY: Int = Resources.FLOOR_OFFSET_Y
  private var heightLimit: Int = Resources.HEIGHT_LIMIT
  private var imageBackground1: Image = Utils.getImage(Resources.IMG_BACKGROUND)
  private var imageBackground2: Image = Utils.getImage(Resources.IMG_BACKGROUND)
  private var castle: Image = Utils.getImage(Resources.IMG_CASTLE)
  private var start: Image = Utils.getImage(Resources.START_ICON)
  private var imageGameOver: Image = Utils.getImage(Resources.IMG_GAME_OVER)
  private var imageGameWon: Image = Utils.getImage(Resources.IMG_GAME_WON)

  private var deathSound: Int = 0
  private var victorySound: Int = 0
  private var startSound: Int = 0

  val factoryCharacter: CharacterFactory = new CharacterFactoryImplementation
  val factoryObject: ObjectFactory = new ObjectFactoryImplementation

  private var characterStrategy: ContactCharacterStrategy = new ContactCharacter
  private var objectStrategy: ContactObjectStrategy = new ContactObject

  var mario: BasicGameCharacter = factoryCharacter.createMario(300, 245)
  var mushroom: BasicGameCharacter = factoryCharacter.createMushroom(800, 263)
  var turtle: BasicGameCharacter = factoryCharacter.createTurtle(950, 243)

  var tunnel1: GameObject = factoryObject.createTunnel(600, 230)
  var tunnel2: GameObject = factoryObject.createTunnel(1000, 230)
  var tunnel3: GameObject = factoryObject.createTunnel(1600, 230)
  var tunnel4: GameObject = factoryObject.createTunnel(1900, 230)
  var tunnel5: GameObject = factoryObject.createTunnel(2500, 230)
  var tunnel6: GameObject = factoryObject.createTunnel(3000, 230)
  var tunnel7: GameObject = factoryObject.createTunnel(3800, 230)
  var tunnel8: GameObject = factoryObject.createTunnel(4500, 230)

  var block1: GameObject = factoryObject.createBlock(400, 180)
  var block2: GameObject = factoryObject.createBlock(1200, 180)
  var block3: GameObject = factoryObject.createBlock(1270, 170)
  var block4: GameObject = factoryObject.createBlock(1340, 160)
  var block5: GameObject = factoryObject.createBlock(2000, 180)
  var block6: GameObject = factoryObject.createBlock(2600, 160)
  var block7: GameObject = factoryObject.createBlock(2650, 180)
  var block8: GameObject = factoryObject.createBlock(3500, 160)
  var block9: GameObject = factoryObject.createBlock(3550, 140)
  var block10: GameObject = factoryObject.createBlock(4000, 170)
  var block11: GameObject = factoryObject.createBlock(4200, 200)
  var block12: GameObject = factoryObject.createBlock(4300, 210)

  var piece1: GameObject = factoryObject.createPiece(402, 145)
  var piece2: GameObject = factoryObject.createPiece(1202, 140)
  var piece3: GameObject = factoryObject.createPiece(1272, 95)
  var piece4: GameObject = factoryObject.createPiece(1342, 40)
  var piece5: GameObject = factoryObject.createPiece(1650, 145)
  var piece6: GameObject = factoryObject.createPiece(2650, 145)
  var piece7: GameObject = factoryObject.createPiece(3000, 135)
  var piece8: GameObject = factoryObject.createPiece(3400, 125)
  var piece9: GameObject = factoryObject.createPiece(4200, 145)
  var piece10: GameObject = factoryObject.createPiece(4600, 40)

  var grow_up_mushroom: GameObject = factoryObject.createGrowUpMushroom(700,270)

  private var piecesTaken : Int = 0
  private var marioIsReallyDead: Boolean = true;

  private var imageCastle: Image = Utils.getImage(Resources.IMG_CASTLE_FINAL)
  private var imageFlag: Image = Utils.getImage(Resources.IMG_FLAG)

  private var gameObjects: mutable.Set[GameObject] = mutable.Set()
  gameObjects += tunnel1 += tunnel2 += tunnel3 += tunnel4 += tunnel5 += tunnel6 += tunnel7 += tunnel8
  gameObjects += block1 += block2 += block3 += block4 += block5 += block6 += block7 += block8 += block9 += block10 += block11 += block12
  gameObjects += grow_up_mushroom

  private var pieces : java.util.ArrayList[GameObject] = new util.ArrayList[GameObject]()
  this.pieces.add(piece1)
  this.pieces.add(piece2)
  this.pieces.add(piece3)
  this.pieces.add(piece4)
  this.pieces.add(piece5)
  this.pieces.add(piece6)
  this.pieces.add(piece7)
  this.pieces.add(piece8)
  this.pieces.add(piece9)
  this.pieces.add(piece10)

  this.setFocusable(true)
  this.requestFocusInWindow
  this.addKeyListener(new Keyboard)

  def getFloorOffsetY: Int = floorOffsetY

  def getHeightLimit: Int = heightLimit

  def getMovement: Int = movement

  def getxPosition: Int = xPosition

  def setBackground2PosX(background2PosX: Int) {
    this.background2PositionX = background2PosX
  }

  def setFloorOffsetY(floorOffsetY: Int) {
    this.floorOffsetY = floorOffsetY
  }

  def setHeightLimit(heightLimit: Int) {
    this.heightLimit = heightLimit
  }

  def setxPosition(xPosition: Int) {
    this.xPosition = xPosition
  }

  def setMovement(mov: Int) {
    this.movement = mov
  }

  def setBackground1PositionX(x: Int) {
    this.background1PositionX = x
  }

  def updateBackgroundOnMovement() {
    if (this.xPosition >= 0 && this.xPosition <= 4600) {
      this.xPosition = this.xPosition + this.movement
      // Moving the screen to give the impression that Mario is walking
      this.background1PositionX = this.background1PositionX - this.movement
      this.background2PositionX = this.background2PositionX - this.movement
    }
    // Flipping between background1 and background2
    if (this.background1PositionX == -800) {
      this.background1PositionX = 800
    }
    else if (this.background2PositionX == -800) {
      this.background2PositionX = 800
    }
    else if (this.background1PositionX == 800) {
      this.background1PositionX = -800
    }
    else if (this.background2PositionX == 800) {
      this.background2PositionX = -800
    }
  }

  override def paintComponent(g: Graphics) {
    super.paintComponent(g)
    val g2: Graphics = g.asInstanceOf[Graphics2D]

    if(startSound == 0){
      Audio.playSound(Resources.AUDIO_START)
      startSound += 1
    }

    for (gameObject <- gameObjects) {
      if(this.mario.isNearby(gameObject) && gameObject.equals(grow_up_mushroom))gameObjects -= grow_up_mushroom
      if (this.mario.isNearby(gameObject) && !gameObject.equals(grow_up_mushroom))this.objectStrategy.contactObjectMario(this.mario, gameObject)
      if (this.mushroom.isNearby(gameObject)) this.objectStrategy.contactObjectNotMario(this.mushroom, gameObject)
      if (this.turtle.isNearby(gameObject)) this.objectStrategy.contactObjectNotMario(this.turtle, gameObject)
    }


    var i: Int = 0
    while (i < pieces.size) {
      {
        if (this.characterStrategy.contactPiece(this.mario,this.pieces.get(i), "Piece")) {
          Audio.playSound(Resources.AUDIO_MONEY)
          piecesTaken += 1
          this.pieces.remove(i)
        }
      }
      {
        i += 1; i - 1
      }
    }

    if (this.mushroom.isNearby(turtle)) {
      this.characterStrategy.contactNotMario(this.mushroom, turtle)
    }
    if (this.turtle.isNearby(mushroom)) {
      this.characterStrategy.contactNotMario(this.turtle, mushroom)
    }
    if (this.mario.isNearby(mushroom)) {
      this.characterStrategy.contactMario(this.mario, mushroom)
    }
    if (this.mario.isNearby(turtle)) {
      this.characterStrategy.contactMario(this.mario, turtle)
    }
    // Moving fixed objects
    this.updateBackgroundOnMovement()
    if (this.xPosition >= 0 && this.xPosition <= 4600) {

      for (gameObject <- gameObjects) {
        gameObject.move()
      }

      for (piece <- pieces) {
        piece.move()
      }
      this.mushroom.move()
      this.turtle.move()
    }
    g2.drawImage(this.imageBackground1, this.background1PositionX, 0, null)
    g2.drawImage(this.imageBackground2, this.background2PositionX, 0, null)
    g2.drawImage(this.castle, 10 - this.xPosition, 95, null)
    g2.drawImage(this.start, 220 - this.xPosition, 234, null)

    for (gameObject <- gameObjects) {
      g2.drawImage(gameObject.getImageObject, gameObject.getCoordinateX, gameObject.getCoordinateY, null)
    }
    for (piece <- pieces) {
      g2.drawImage(piece.imageOnMovement, piece.getCoordinateX, piece.getCoordinateY, null)
    }

    g2.drawImage(this.imageFlag, Resources.FLAG_X_POS - this.xPosition, Resources.FLAG_Y_POS, null)
    g2.drawImage(this.imageCastle, Resources.CASTLE_X_POS - this.xPosition, Resources.CASTLE_Y_POS, null)

    if (this.mario.isJumping) g2.drawImage(this.mario.doJump(), this.mario.getCoordinateX, this.mario.getCoordinateY, null)
    else g2.drawImage(this.mario.imageOfWalkingThings(Resources.IMGP_CHARACTER_MARIO, Resources.MARIO_FREQUENCY), this.mario.getCoordinateX, this.mario.getCoordinateY, null)

    if (this.mushroom.isAlive) g2.drawImage(this.mushroom.imageOfWalkingThings(Resources.IMGP_CHARACTER_MUSHROOM, Resources.MUSHROOM_FREQUENCY), this.mushroom.getCoordinateX, this.mushroom.getCoordinateY, null)
    else g2.drawImage(this.mushroom.deadImage, this.mushroom.getCoordinateX, this.mushroom.getCoordinateY + Resources.MUSHROOM_DEAD_OFFSET_Y, null)

    if (this.turtle.isAlive) g2.drawImage(this.turtle.imageOfWalkingThings(Resources.IMGP_CHARACTER_TURTLE, Resources.TURTLE_FREQUENCY), this.turtle.getCoordinateX, this.turtle.getCoordinateY, null)
    else g2.drawImage(this.turtle.deadImage, this.turtle.getCoordinateX, this.turtle.getCoordinateY + Resources.TURTLE_DEAD_OFFSET_Y, null)

    if(piecesTaken.equals(10)){
      g2.drawImage(imageGameWon, Resources.GAME_WON_OFFSET_X, Resources.GAME_WON_OFFSET_Y, null)
      if(victorySound == 0) {
        Audio.playSound(Resources.AUDIO_VICTORY)
        victorySound += 1
      }
      marioIsReallyDead = false;
      mario.setAlive(false)
    }

    if (!this.mario.isAlive && marioIsReallyDead) {
      g2.drawImage(imageGameOver, Resources.GAME_OVER_OFFSET_X, Resources.GAME_OVER_OFFSET_Y, null)
      if(deathSound == 0){
        Audio.playSound(Resources.AUDIO_DEATH)
        deathSound+=1
      }

    }
  }
}