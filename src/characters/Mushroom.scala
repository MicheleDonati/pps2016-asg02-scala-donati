package characters

import java.awt.Image
import factory.CharacterFactory
import objects.GameObject
import objects.Piece
import strategies.ContactCharacterStrategy
import utils.Resources
import utils.Utils

class Mushroom(val x: Int, val y: Int) extends BasicGameCharacter(x, y, Resources.MUSHROOM_WIDTH, Resources.MUSHROOM_HEIGHT) with Runnable {
  this.setToRight(true)
  this.setMoving(true)
  private var offsetX: Int = Resources.MUSHROOM_OFFSET_COORDINATE_X
  private var mushroomImage: Image =  Utils.getImage(Resources.IMG_MUSHROOM_DEFAULT)
  final private val PAUSE: Int = 15
  val fewSecondsMushroom: Thread = new Thread(this)
  fewSecondsMushroom.start()

  //getters
  def getMushroomImage: Image = mushroomImage

  def moveMushroom() {
    this.offsetX = if (isToRight) 1
    else -1
    this.setCoordinateX(this.getCoordinateX + this.offsetX)
  }

  def run() {
    while (true) {
      {
        if (this.isAlive) {
          this.move()
          try {
            Thread.sleep(PAUSE)
          }
          catch {
            case e: InterruptedException =>
          }
        }
      }
    }
  }

  def isJumping: Boolean = false

  def setJumping(jumping: Boolean) {
  }

  def doJump(): Image = null

  def deadImage: Image = {
    Utils.getImage(if (this.isToRight) Resources.IMG_MUSHROOM_DEAD_DX
                    else Resources.IMG_MUSHROOM_DEAD_SX)
  }

  def setOffsetX(x: Int) {
    this.offsetX = x
  }
}


