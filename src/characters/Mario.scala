package characters

import java.awt.Image
import game.Main
import objects.GameObject
import objects.Piece
import strategies.ContactCharacterStrategy
import utils.Resources
import utils.Utils

class Mario(val x: Int, val y: Int) extends BasicGameCharacter(x, y, Resources.WIDTH, Resources.HEIGHT) {
  private var imageMario: Image = Utils.getImage(Resources.IMG_MARIO_DEFAULT)
  private var jumping: Boolean = false
  private var jumpingExtent: Int = Resources.EXTENSION_OF_JUMP

  def isJumping: Boolean = jumping

  def setJumping(jumping: Boolean) {
    this.jumping = jumping
  }

  def doJump(): Image = {
    var stringToDefineJumpImageInGame: String = null
    this.jumpingExtent += 1
    if (this.jumpingExtent < Resources.JUMPING_LIMIT) {
      if (this.getCoordinateY > Main.scene.getHeightLimit) this.setCoordinateY(this.getCoordinateY - 4)
      else this.jumpingExtent = Resources.JUMPING_LIMIT
      stringToDefineJumpImageInGame = if (this.isToRight) Resources.IMG_MARIO_SUPER_DX
      else Resources.IMG_MARIO_SUPER_SX
    }
    else if (this.getCoordinateY + this.getHeight < Main.scene.getFloorOffsetY) {
      this.setCoordinateY(this.getCoordinateY + 1)
      stringToDefineJumpImageInGame = if (this.isToRight) Resources.IMG_MARIO_SUPER_DX
      else Resources.IMG_MARIO_SUPER_SX
    }
    else {
      stringToDefineJumpImageInGame = if (this.isToRight) Resources.IMG_MARIO_ACTIVE_DX
      else Resources.IMG_MARIO_ACTIVE_SX
      this.jumping = false
      this.jumpingExtent = 0
    }
    Utils.getImage(stringToDefineJumpImageInGame)
  }

  def deadImage:Image = null

  def setOffsetX(x: Int) {
  }
}
