package characters

import java.awt.Image

trait GameCharacter {
  def imageOfWalkingThings(name: String, frequency: Int): Image
}

