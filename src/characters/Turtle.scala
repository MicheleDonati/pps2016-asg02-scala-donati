package characters

import java.awt.Image
import objects.GameObject
import objects.Piece
import strategies.ContactCharacterStrategy
import utils.Resources
import utils.Utils

class Turtle(val X: Int, val Y: Int) extends BasicGameCharacter(X, Y, Resources.TURTLE_WIDTH, Resources.TURTLE_HEIGHT) with Runnable {
  super.setToRight(true)
  super.setMoving(true)
  private var dxTurtle: Int = 1
  private var imageTurtle: Image = Utils.getImage(Resources.IMG_TURTLE_IDLE)
  final private val PAUSE: Int = 15
  val chronoTurtle: Thread = new Thread(this)
  chronoTurtle.start()

  def getTurtleImage: Image = imageTurtle

  override def move() {
    this.dxTurtle = if (isToRight) 1
    else -1
    super.setCoordinateX(super.getCoordinateX + this.dxTurtle)
  }

  def run() {
    while (true) {
      {
        if (this.isAlive) {
          this.move()
          try {
            Thread.sleep(PAUSE)
          }
          catch {
            case e: InterruptedException => {
            }
          }
        }
      }
    }
  }

  def deadImage: Image = Utils.getImage(Resources.IMG_TURTLE_DEAD)

  def isJumping: Boolean = false

  def setJumping(jumping: Boolean) {
  }

  def doJump(): Image = null

  def setOffsetX(x: Int) {
    this.dxTurtle = x
  }
}

