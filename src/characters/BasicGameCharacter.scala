package characters

import java.awt.Image
import game.Main
import objects.GameObject
import strategies.ContactCharacterStrategy
import utils.Resources
import utils.Utils

abstract class BasicGameCharacter(var coordinateX: Int, var coordinateY: Int, var width: Int, var height: Int) extends GameCharacter {

  var counter: Int = Resources.STARTER_COUNTER
  protected var moving: Boolean = false
  protected var toRight: Boolean = true
  protected var alive: Boolean = true
  var characterStrategy: ContactCharacterStrategy = _

  def isJumping: Boolean

  def setJumping(jumping: Boolean)

  def doJump(): Image

  def deadImage: Image

  def setOffsetX(x: Int)

  def getCoordinateX: Int = coordinateX

  def getCoordinateY: Int = coordinateY

  def getWidth: Int = width

  def getHeight: Int = height

  def getCounter: Int = counter

  def isAlive: Boolean = alive

  def isMoving: Boolean = moving

  def isToRight: Boolean = toRight

  def setAlive(alive: Boolean) {
    this.alive = alive
  }

  def setCoordinateX(x: Int) {
    this.coordinateX = x
  }

  def setCoordinateY(y: Int) {
    this.coordinateY = y
  }

  def setMoving(moving: Boolean) {
    this.moving = moving
  }

  def setToRight(toRight: Boolean) {
    this.toRight = toRight
  }

  def setCounter(counter: Int) {
    this.counter = counter
  }

  def imageOfWalkingThings(name: String, frequency: Int): Image = {
    val stringForChooseInGameImage: String = Resources.IMG_BASE + name + (if (!this.moving || {
      this.counter += 1; this.counter
    } % frequency == 0) Resources.IMGP_STATUS_ACTIVE
    else Resources.IMGP_STATUS_NORMAL) + (if (this.toRight) Resources.IMGP_DIRECTION_DX
    else Resources.IMGP_DIRECTION_SX) + Resources.IMG_EXT
    Utils.getImage(stringForChooseInGameImage)
  }

  def move() {
    if (Main.scene.getxPosition >= 0) {
      this.coordinateX = this.coordinateX - Main.scene.getMovement
    }
  }

  def hitAhead(`object`: GameObject): Boolean = {
    if (this.coordinateX + this.width < `object`.getCoordinateX || this.coordinateX + this.width > `object`.getCoordinateX + 5 || this.coordinateY + this.height <= `object`.getCoordinateY || this.coordinateY >= `object`.getCoordinateY + `object`.getHeight) {
      false
    }
    else true
  }

  def hitBack(`object`: GameObject): Boolean = {
    if (this.coordinateX > `object`.getCoordinateX + `object`.getWidth || this.coordinateX + this.width < `object`.getCoordinateX + `object`.getWidth - 5 || this.coordinateY + this.height <= `object`.getCoordinateY || this.coordinateY >= `object`.getCoordinateY + `object`.getHeight) {
      false
    }
    else true
  }

  def hitBelow(`object`: GameObject): Boolean = {
    if (this.coordinateX + this.width < `object`.getCoordinateX + 5 || this.coordinateX > `object`.getCoordinateX + `object`.getWidth - 5 || this.coordinateY + this.height < `object`.getCoordinateY || this.coordinateY + this.height > `object`.getCoordinateY + 5) {
      false
    }
    else true
  }

  def hitAbove(`object`: GameObject): Boolean = {
    if (this.coordinateX + this.width < `object`.getCoordinateX + 5 || this.coordinateX > `object`.getCoordinateX + `object`.getWidth - 5 || this.coordinateY < `object`.getCoordinateY + `object`.getHeight || this.coordinateY > `object`.getCoordinateY + `object`.getHeight + 5) {
      false
    }
    else true
  }

  def hitAhead(character: BasicGameCharacter): Boolean = {
    if (this.isToRight) {
      if (this.coordinateX + this.width < character.getCoordinateX || this.coordinateX + this.width > character.getCoordinateX + 5 || this.coordinateY + this.height <= character.getCoordinateY || this.coordinateY >= character.getCoordinateY + character.getHeight) {
        false
      }
      else {
        true
      }
    }
    else {
      false
    }
  }

  def hitBack(character: BasicGameCharacter): Boolean = {
    if (this.coordinateX > character.getCoordinateX + character.getWidth || this.coordinateX + this.width < character.getCoordinateX + character.getWidth - 5 || this.coordinateY + this.height <= character.getCoordinateY || this.coordinateY >= character.getCoordinateY + character.getHeight) return false
    true
  }

  def hitBelow(character: BasicGameCharacter): Boolean = {
    if (this.coordinateX + this.width < character.getCoordinateX || this.coordinateX > character.getCoordinateX + character.getWidth || this.coordinateY + this.height < character.getCoordinateY || this.coordinateY + this.height > character.getCoordinateY) return false
    true
  }

  def isNearby(character: BasicGameCharacter): Boolean = {
    if ((this.coordinateX > character.getCoordinateX - Resources.PROXIMITY_MARGIN && this.coordinateX < character.getCoordinateX + character.getWidth + Resources.PROXIMITY_MARGIN) || (this.coordinateX + this.width > character.getCoordinateX - Resources.PROXIMITY_MARGIN && this.coordinateX + this.width < character.getCoordinateX + character.getWidth + Resources.PROXIMITY_MARGIN)) return true
    false
  }

  def isNearby(`object`: GameObject): Boolean = {
    if ((this.coordinateX > `object`.getCoordinateX - Resources.PROXIMITY_MARGIN && this.coordinateX < `object`.getCoordinateX + `object`.getWidth + Resources.PROXIMITY_MARGIN) || (this.getCoordinateX + this.width > `object`.getCoordinateX - Resources.PROXIMITY_MARGIN && this.coordinateX + this.width < `object`.getCoordinateX + `object`.getWidth + Resources.PROXIMITY_MARGIN)) return true
    false
  }
}

