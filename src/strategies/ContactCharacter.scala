package strategies

import characters.BasicGameCharacter
import objects.GameObject

class ContactCharacter extends ContactCharacterStrategy{

  override def contactMario(mario: BasicGameCharacter, character: BasicGameCharacter) {
    if (mario.hitAhead(character) || mario.hitBack(character)) {
      if (character.isAlive) {
        mario.setMoving(false)
        mario.setAlive(false)
      }
      else mario.setAlive(true)
    }
    else if (mario.hitBelow(character)) {
      character.setMoving(false)
      character.setAlive(false)
    }
  }

  override def contactNotMario(notMario: BasicGameCharacter, character: BasicGameCharacter) {
    if (notMario.hitAhead(character) && notMario.isToRight) {
      notMario.setToRight(false)
      notMario.setOffsetX(-1)
    }
    else if (notMario.hitBack(character) && !notMario.isToRight) {
      notMario.setToRight(true)
      notMario.setOffsetX(-1)
    }
  }

  def contactPiece(mario: BasicGameCharacter, piece: GameObject, isAPiece: String): Boolean = {
    if (mario.hitBack(piece) || mario.hitAbove(piece) || mario.hitAhead(piece) || mario.hitBelow(piece)) return true
    false
  }

  override def executeContact(callback:(BasicGameCharacter, BasicGameCharacter) => Unit, firstCharacter: BasicGameCharacter, secondCharacter:BasicGameCharacter): Unit = callback(firstCharacter, secondCharacter)
}
