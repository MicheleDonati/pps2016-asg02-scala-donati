package strategies

import characters.BasicGameCharacter
import objects.GameObject

trait ContactCharacterStrategy {
  def contactMario(characterItself: BasicGameCharacter, character: BasicGameCharacter)

  def contactNotMario(characterItself: BasicGameCharacter, character: BasicGameCharacter)

  def executeContact(callback:(BasicGameCharacter, BasicGameCharacter) => Unit, firstCharacter: BasicGameCharacter, secondCharacter:BasicGameCharacter): Unit = callback(firstCharacter, secondCharacter)

  def contactPiece(mario: BasicGameCharacter, piece: GameObject, isAPiece: String): Boolean

}