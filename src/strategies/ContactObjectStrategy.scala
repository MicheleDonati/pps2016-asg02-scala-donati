package strategies

import characters.BasicGameCharacter
import objects.GameObject

trait ContactObjectStrategy {
  def contactObjectMario(character: BasicGameCharacter, `object`: GameObject)
  def contactObjectNotMario(character: BasicGameCharacter, `object`: GameObject)
}